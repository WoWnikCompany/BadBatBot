import asyncio
import logging
import os
from sys import platform

from src.Main import main

from dotenv import load_dotenv

if os.path.exists('.env'):
    load_dotenv(".env")

if __name__ == "__main__":
    if platform[:3] == "win":
        asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())

    logging.basicConfig(level=logging.NOTSET)

    loop = asyncio.get_event_loop()
    asyncio.ensure_future(main(), loop=loop)
    loop.run_forever()
