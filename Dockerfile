FROM python:3.10-slim

RUN mkdir -p /app
WORKDIR /app

COPY . /app

EXPOSE 3000
ENV PORT 3000
ENV HOSTNAME "0.0.0.0"

RUN pip install --no-cache-dir -r requirements.txt
CMD ["python", "main.py"]
