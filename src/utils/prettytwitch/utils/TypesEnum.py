class TypesEnum:
    UNDEFINED = 'UNDEFINED'

    # System Messages
    CONNECTION = 'CONNECTION'
    USER_LIST = 'USER_LIST'
    USER_LIST_END = 'USER_LIST_END'
    RECONNECT = 'RECONNECT'
    PING = 'PING'
    PONG = 'PONG'

    # Invalid IRC Command
    INVALID_CMD = 'INVALID_CMD'

    # Twitch specific IRC Requests
    CAPABILITY_REG = 'CAPABILITY_REG'

    # Generic
    JOIN = 'JOIN'
    PART = 'PART'
    PRIVMSG = 'PRIVMSG'

    # Capability: Membership
    OP = 'OP'
    DEOP = 'DEOP'

    # Capability: Commands
    NOTICE = 'NOTICE'
    HOSTSTART = 'HOSTSTART'
    HOSTEND = 'HOSTEND'
    TIMEOUT = 'TIMEOUT'
    BAN = 'BAN'
    CLEARCHAT = 'CLEARCHAT'
    USERSTATE = 'USERSTATE'
    ROOMSTATE = 'ROOMSTATE'
    USERNOTICE = 'USERNOTICE'
    SUB = 'SUB'
    GLOBALUSERSTATE = 'GLOBALUSERSTATE'
