import re

from src.utils.prettytwitch.utils.TypesEnum import TypesEnum

TYPES = {
    # System Messages
    TypesEnum.CONNECTION: re.compile(r'^:tmi\.twitch\.tv ([0-9]+) (\w+) :(.+)\r\n'),
    TypesEnum.USER_LIST: re.compile(r'^:\w+\.tmi\.twitch\.tv 353 (\w+) = #(\w+) :(.+)\r\n'),
    TypesEnum.USER_LIST_END: re.compile(r'^:\w+\.tmi\.twitch\.tv 366 (\w+) #(\w+) :End of /NAMES list\r\n'),
    TypesEnum.RECONNECT: re.compile(r'^:tmi\.twitch\.tv RECONNECT\r\n'),
    TypesEnum.PING: re.compile(r'PING :tmi.twitch.tv'),
    TypesEnum.PONG: re.compile(r':tmi.twitch.tv PONG tmi.twitch.tv :tmi.twitch.tv'),

    # Invalid IRC Command
    TypesEnum.INVALID_CMD: re.compile(r'^:tmi\.twitch\.tv 421 (\w+) (\w+):Unknown command\r\n'),

    # Twitch specific IRC Requests
    TypesEnum.CAPABILITY_REG: re.compile(r'^:tmi\.twitch\.tv CAP \* ([AN][CA]K) :(.+)\r\n'),

    # Generic
    TypesEnum.JOIN: re.compile(r'^:(\w+)!\w+@\w+\.tmi\.twitch\.tv JOIN #(\w+)\r\n'),
    TypesEnum.PART: re.compile(r'^:(\w+)!\w+@\w+\.tmi\.twitch\.tv PART #(\w+)\r\n'),
    TypesEnum.PRIVMSG: re.compile(r'^:(\w+)!\w+@\w+\.tmi\.twitch\.tv PRIVMSG #(\w+) :(.*)\r\n'),

    # Capability: Membership
    TypesEnum.OP: re.compile(r'^:jtv MODE #(\w+) \+o (\w)\r\n'),
    TypesEnum.DEOP: re.compile(r'^:jtv MODE #(\w+) -o (\w)\r\n'),

    # Capability: Commands
    TypesEnum.NOTICE: re.compile(r'^@msg-id=(\w+) :tmi\.twitch\.tv NOTICE #(\w+) :(.+)\r\n'),
    TypesEnum.HOSTSTART: re.compile(r'^:tmi\.twitch\.tv HOSTTARGET #(\w+) :(\w+) ([0-9]+)\r\n'),
    TypesEnum.HOSTEND: re.compile(r'^:tmi\.twitch\.tv HOSTTARGET #(\w+) :- ([0-9]+)\r\n'),
    TypesEnum.BAN: re.compile(r'^:tmi\.twitch\.tv CLEARCHAT #(\w+) :(\w+)\r\n'),
    TypesEnum.CLEARCHAT: re.compile(r'^:tmi\.twitch\.tv CLEARCHAT #(\w+)\r\n'),
    TypesEnum.USERSTATE: re.compile(r'^:tmi\.twitch\.tv USERSTATE #(\w+)\r\n'),
    TypesEnum.ROOMSTATE: re.compile(r'^:tmi\.twitch\.tv ROOMSTATE #(\w+)\r\n'),
    TypesEnum.USERNOTICE: re.compile(r'^:tmi\.twitch\.tv USERNOTICE #(\w+)\r\n'),
    TypesEnum.SUB: re.compile(r'^:tmi\.twitch\.tv USERNOTICE #(\w+) :(.+)\r\n'),
    TypesEnum.GLOBALUSERSTATE: re.compile(r'^:tmi\.twitch\.tv GLOBALUSERSTATE\r\n')
}

PMESSAGE_FORMAT = {
    # System Messages
    TypesEnum.CONNECTION: 'SERVER MESSAGE: {0.message}',
    TypesEnum.USER_LIST: 'LIST OF ONLINE USERS FOR #{0.channel}: {0.pviewers}',
    TypesEnum.USER_LIST_END: '--End of the viewer list--',
    TypesEnum.RECONNECT: 'ATTENTION: SERVERS ARE RESTARTING! RECONNECT IMMEDIATELY!',
    TypesEnum.PING: 'PING FROM TWITCH',
    TypesEnum.PONG: 'PONG FROM TWITCH',

    # Invalid IRC Command
    TypesEnum.INVALID_CMD: 'COMMAND \'{0.command}\' IS INVALID.',

    # Twitch specific IRC Requests
    TypesEnum.CAPABILITY_REG: 'YOUR REQUEST FOR {0.capability} WAS {0.psuccess}',

    # Generic
    TypesEnum.JOIN: 'USER {0.user} HAS JOINED #{0.channel}',
    TypesEnum.PART: 'USER {0.user} HAS LEFT #{0.channel}',
    TypesEnum.PRIVMSG: '#{0.channel} {0.user}: {0.message}',

    # Capability: Membership
    TypesEnum.OP: 'USER {0.user} HAS BEEN MODDED IN #{0.channel}',
    TypesEnum.DEOP: 'USER {0.user} HAS BEEN UNMODDED IN #{0.channel}',

    # Capability: Commands
    TypesEnum.NOTICE: 'NOTICE! #{0.channel}: {0.message}',
    TypesEnum.HOSTSTART: '{0.channel} IS NOW HOSTING {0.target_channel} WITH {0.viewers} VIEWERS',
    TypesEnum.HOSTEND: '{0.channel} HAS FINISHED HOSTING WITH {0.viewers} VIEWERS',
    TypesEnum.BAN: '{0.user} HAS BEEN BANNED FROM #{0.channel}',
    TypesEnum.TIMEOUT: '{0.user} HAS BEEN TIMED OUT FROM #{0.channel} FOR {0.ban_duration} SECONDS',
    TypesEnum.USERSTATE: 'YOU HAVE SUCCESSFULLY LOGGED IN',
    TypesEnum.ROOMSTATE: '#{0.channel} SETTINGS HAVE CHANGED',
    TypesEnum.USERNOTICE: 'YOU ARE BEING RAIDED BY {0.channel}',
    TypesEnum.SUB: 'SOMEONE HAVE SUBSCRIBED TO {0.channel}: {0.message}',
    TypesEnum.GLOBALUSERSTATE: 'YOU HAVE SUCCESSFULLY LOGGED IN'
}
