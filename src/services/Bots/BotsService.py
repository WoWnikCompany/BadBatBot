import asyncio
from typing import List

from src.services.Bots.PyBot import PyBot


class BotsService:
    list_of_bots: List[PyBot] = []

    async def run_bots(self):
        from src.kostils import bots

        self.list_of_bots = [PyBot(bot_name=bot.NICK, password=bot.PASS) for bot in bots]
        tasks = [bot.connect() for bot in self.list_of_bots]
        await asyncio.wait(tasks)

    async def join_from_list(self, channels: List[str]):
        tasks = [self.join_channels(bot, [bot.bot_name, "wownik"] + channels) for bot in self.list_of_bots]
        await asyncio.wait(tasks)

    @staticmethod
    async def join_channels(bot: PyBot, channels: List[str]):
        # Обязательно последовательно!
        for channel in channels:
            await bot.join_channel(channel.lower())

    async def run(self):
        from src.kostils import channels

        await self.run_bots()
        await self.join_from_list(channels)
